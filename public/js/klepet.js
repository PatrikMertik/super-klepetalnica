// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var channel = besede.join(' ');
      this.spremeniKanal(channel);
      break;
    case 'barva':
      besede.shift();
      document.querySelector('#sporocila').style.backgroundColor=besede[0];
      document.querySelector('#kanal').style.backgroundColor=besede[0];
      
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
        besede.shift();
        var dodajanje = '';
        var parametri=besedilo.split('\"');
        var besedilo=besede.join(' ');
        var linki=besedilo.match(/(\bhttp\S+\.(?:jpg|png|gif)\b)/g);
      	if (linki.length!=0) {
        		for (var i = 1; i <= linki.length; i++) {  
        			dodajanje = (' <br/ ><img src=\"' +  linki[i] + '\" style=\"width: 200px; padding-left: 20px;\"></img>')+dodajanje;
        		}
      	}
        if (parametri) {
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3] + dodajanje});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3] + dodajanje;
        } else {
          sporocilo = 'Neznan ukaz';
        }
    break;
    default:
      sporocilo = 'Neznan ukaz.';
    break;
  };

  return sporocilo;
};